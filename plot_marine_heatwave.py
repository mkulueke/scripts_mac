
# coding: utf-8

# In[2]:


import datetime
import matplotlib.pyplot as plt
import numpy as np
from netCDF4 import Dataset


def serial_date_to_string(srl_no):
    new_date = datetime.datetime(1800,1,1,0,0) + datetime.timedelta(srl_no)
    return new_date.strftime("%Y")


dataset = Dataset('/Users/marco/Documents/data/sst_DJF_seasmean_106to116E_26to36S_land_masked.nc')
#print(dataset.variables.keys())
#print(dataset.variables['sst'])
#print(dataset.variables['time'])
sst = dataset.variables['sst'][:, 0, 0]
sst_1983_2000_mean = np.zeros(sst.size)
sst_1983_2000_mean[:] = np.mean(sst[0: 17])
time = dataset.variables['time'][:]
time_date = serial_date_to_string(time[0])
x = np.arange(1983, 2018, 1)

plt.plot(x, sst, label='DJF SST Mean')
plt.plot(x, sst_1983_2000_mean, label='1983-2000 DJF SST Mean')
plt.legend(loc='lower left')
plt.title("DJF Seasonal Mean of SST West Australiasn Coast")
plt.xlabel("Year")
plt.ylabel("SST $^o$C")
plt.show()

