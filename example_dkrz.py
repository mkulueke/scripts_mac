
# coding: utf-8

# # Nio and Ngl

# In[1]:


'''
 DKRZ PyNGL Script: PyNGL_rectilinear_contour.py

 Description:       Python script using PyNGL Python module
 
                   - contour plot on map (rectilinear data)

 2015-06-04  meier-fleischer(at)dkrz.de
'''
import numpy,sys,os
import Nio
import Ngl

#--  define variables
diri   = "/Users/marco/Documents/data/"                                  #-- data directory
fname  = "sst_DJF_1971-2000_lt_seasmean.nc"              #-- data file name

minval =  250.                                 #-- minimum contour level
maxval =  315                                  #-- maximum contour level
inc    =    5.                                 #-- contour level spacing
ncn    = (maxval-minval)/inc + 1               #-- number of contour levels

#--  open file and read variables
f      =  Nio.open_file(diri + fname,"r")      #-- open data file
temp   =  f.variables["sst"][0,::-1,:]       #-- first time step, reverse latitude
lat    =  f.variables["lat"][::-1]             #-- reverse latitudes
lon    =  f.variables["lon"][:]                #-- all longitudes

tempac =  Ngl.add_cyclic(temp[:,:])

#-- open a workstation
wkres                  =  Ngl.Resources()      #-- generate an res object for workstation
wkres.wkWidth          =  1024                 #-- plot resolution 2500 pixel width
wkres.wkHeight         =  1024                 #-- plot resolution 2500 pixel height
wkres.wkColorMap       = "rainbow"             #-- choose colormap
wks_type               = "png"                 #-- graphics output type
wks                    =  Ngl.open_wks(wks_type,"Py_rectilinear_contour",wkres)  #-- open workstation

#-- set resources
res                    =  Ngl.Resources()      #-- generate an resource object for plot

if hasattr(f.variables["sst"],"long_name"):
   res.tiMainString = f.variables["sst"].long_name  #-- if long_name attribute exists use it as title

#-- viewport resources
res.vpXF                  =  0.1               #-- start x-position of viewport
res.vpYF                  =  0.9               #-- start y-position of viewport
res.vpWidthF              =  0.7               #-- width of viewport
res.vpHeightF             =  0.7               #-- height of viewport

#-- contour resources
res.cnFillOn              =  True              #-- turn on contour fill
res.cnLineLabelsOn        =  False             #-- turn off line labels
res.cnInfoLabelOn         =  False             #-- turn off info label
res.cnLevelSelectionMode  = "ManualLevels"     #-- select manual level selection mode
res.cnMinLevelValF        =  minval            #-- minimum contour value
res.cnMaxLevelValF        =  maxval            #-- maximum contour value
res.cnLevelSpacingF       =  inc               #-- contour increment

#-- grid data information resources
res.sfXCStartV            =  float(min(lon))   #-- x-axis location of 1st element lon
res.sfXCEndV              =  float(max(lon))   #-- x-axis location of last element lon
res.sfYCStartV            =  float(min(lat))   #-- y-axis location of 1st element lat
res.sfYCEndV              =  float(max(lat))   #-- y-axis location of last element lat

#-- labelbar resources
res.pmLabelBarDisplayMode = "Always"           #-- turn on the label bar
res.lbOrientation         = "Horizontal"       #-- labelbar orientation

#-- draw contours over a map
map = Ngl.contour_map(wks,temp[:,:],res)

#-- done
Ngl.end()


# # Matplotlib

# In[ ]:


"""
DKRZ matplotlib script:  matplotlib_contour_filled.py

   - filled contour over map plot
   - rectilinear grid (lat/lon)
   - colorbar
   
   12.06.15  meier-fleischer(at)dkrz.de
"""
from   mpl_toolkits.basemap import Basemap, cm
import matplotlib.pyplot as plt
from   netCDF4 import Dataset as open_ncfile
import numpy as np

#-- open netcdf file
nc = open_ncfile('/Users/marco/Documents/data/sst_DJF_1971-2000_lt_seasmean.nc')

#-- read variable
var = nc.variables['sst'][0,:,:]
lat = nc.variables['lat'][:]
lon = nc.variables['lon'][:]

#-- create figure and axes instances
dpi = 100
fig = plt.figure(figsize=(1100/dpi, 1100/dpi), dpi=dpi)
ax  = fig.add_axes([0.1,0.1,0.8,0.9])

#-- create map
map = Basemap(projection='cyl',llcrnrlat= -90.,urcrnrlat= 90.,              resolution='c',  llcrnrlon=-180.,urcrnrlon=180.)

#-- draw coastlines, state and country boundaries, edge of map
map.drawcoastlines()
map.drawstates()
map.drawcountries()

#-- create and draw meridians and parallels grid lines
map.drawparallels(np.arange( -90., 90.,30.),labels=[1,0,0,0],fontsize=10)
map.drawmeridians(np.arange(-180.,180.,30.),labels=[0,0,0,1],fontsize=10)

#-- convert latitude/longitude values to plot x/y values
x, y = map(*np.meshgrid(lon,lat))

#-- contour levels
clevs = np.arange(210,320,5)

#-- draw filled contours
cnplot = map.contourf(x,y,var,clevs,cmap=plt.cm.jet)

#-- add colorbar
cbar = map.colorbar(cnplot,location='bottom',pad="10%")      #-- pad: distance between map and colorbar
cbar.set_label('deg K')                                      #-- add colorbar title string

#-- add plot title
plt.title('Temperature')

#-- displa on screen
#plt.show()

#-- maximize and save PNG file
plt.savefig('plot_matplotlib_contour_filled_rect.png', bbox_inches='tight', dpi=dpi)

